"""
定义文件路径配置信息
"""
train_raw_file = './data/data_cat10_annotated_train.txt'
eval_raw_file = './data/data_cat10_annotated_eval.txt'
test_raw_file = './data/data_cat10_annotated_test.txt'
label_ids_file = './data/id_label.txt'
train_data_file = './data/train.csv'
eval_data_file = './data/eval.csv'
test_data_file = './data/test.csv'
